import { RequestHandler, Router } from "express";
import { FarmsV1Controller } from "modules/farms/farms.v1.controller";
import { authMiddleware } from '../middlewares/auth.middleware'

const router = Router();
const farmsController = new FarmsV1Controller();

router.post("/v1/farms/", authMiddleware, farmsController.createOne.bind(farmsController) as RequestHandler);
router.delete("/v1/farms/:id", authMiddleware, farmsController.deleteOne.bind(farmsController) as RequestHandler);
router.get("/v1/farms/", authMiddleware, farmsController.getAll.bind(farmsController) as RequestHandler);

export default router;
