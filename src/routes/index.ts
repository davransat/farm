import { Router } from "express";
import auth from "./auth.routes";
import user from "./user.routes";
import farm from "./farm.routes";

const routes = Router();

routes.use("/", auth);
routes.use("/", user);
routes.use("/", farm);

export default routes;
