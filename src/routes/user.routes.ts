import { RequestHandler, Router } from "express";
import { UsersV1Controller } from "modules/users/users.v1.controller";

const router = Router();
const usersController = new UsersV1Controller();

router.post("/v1/users", usersController.create.bind(usersController) as RequestHandler);

export default router;
