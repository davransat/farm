import { RequestHandler, Router } from "express";
import { AuthV1Controller } from "modules/auth/auth.v1.controller";

const router = Router();
const authController = new AuthV1Controller();

router.post("/v1/auth/login", authController.login.bind(authController) as RequestHandler);

export default router;
