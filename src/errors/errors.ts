export class UnprocessableEntityError extends Error {}
export class ForbiddenError extends Error {}
export class BadRequestError extends Error {}
export class UnauthorizedError extends Error {}
