import { ratioFromPercentage } from './ratio-from-percentage.util'

describe('ratio-from-percentage', () => {
  describe('ratioFromPercentage', () =>{
    it('should be converted to ratio', async () => {
      expect(ratioFromPercentage(100)).toBe(1)
      expect(ratioFromPercentage(10)).toBe(0.1)
    })
  })
})
