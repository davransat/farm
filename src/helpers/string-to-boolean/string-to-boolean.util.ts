export function stringToBoolean <T>(value: T): boolean | T
export function stringToBoolean <T>(value: T, throwError: true): boolean
export function stringToBoolean <T>(value: T, throwError?: true): boolean | T {
  switch (value) {
    case '1':
    case 'true':
    case true: {
      return true
    }
    case '0':
    case 'false':
    case false: {
      return false
    }
    case null:
    case undefined: {
      return value
    }
    default: {
      if (throwError) {
        throw new Error('Failed to convert string to boolean')
      }
      return value
    }
  }
}
