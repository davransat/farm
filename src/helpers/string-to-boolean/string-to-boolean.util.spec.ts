import { stringToBoolean } from './string-to-boolean.util'

describe('string to boolean converter', () => {
  describe('stringToBoolean', () =>{
    it('should be converted to boolean', async () => {
      expect(stringToBoolean(1)).toBeTruthy()
      expect(stringToBoolean('true')).toBeTruthy()
      expect(stringToBoolean(true)).toBeTruthy()

      expect(stringToBoolean(0)).toBeFalsy()
      expect(stringToBoolean('false')).toBeFalsy()
      expect(stringToBoolean(false)).toBeFalsy()
    })

    it('should return null and undefined values without converting', async () => {
      expect(stringToBoolean(null)).toBe(null)
      expect(stringToBoolean(undefined)).toBe(undefined)

    })

    it('should return non boolean value without converting', async () => {
      expect(stringToBoolean('str')).toBe('str')
    })

    it('should throw error on non boolean value', async () => {
      expect(() => stringToBoolean('str', true)).toThrow()
    })
  })
})
