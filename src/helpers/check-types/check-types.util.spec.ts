import { isArray, isNumber } from './check-types.util'

describe('check types utils', () => {
  describe('isNumber', () =>{
    it('should be falsy', async () => {
      expect(isNumber('1')).toBeFalsy()
      expect(isNumber(null)).toBeFalsy()
      expect(isNumber(undefined)).toBeFalsy()
      expect(isNumber(true)).toBeFalsy()
    })
    it('should be truthy', async () => {
      expect(isNumber(1)).toBeTruthy()
    })
  })
  describe('isArray', () =>{
    it('should be falsy', async () => {
      expect(isArray('1')).toBeFalsy()
      expect(isArray(null)).toBeFalsy()
      expect(isArray(undefined)).toBeFalsy()
      expect(isArray(true)).toBeFalsy()
      expect(isArray({})).toBeFalsy()
    })
    it('should be truthy', async () => {
      expect(isArray([])).toBeTruthy()
    })
  })
})
