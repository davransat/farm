export const isNumber = (arg: any): arg is number => Object.prototype.toString.call(arg) === '[object Number]'
export const isArray = <T>(arg: T[] | unknown): arg is T[] => Object.prototype.toString.call(arg) === '[object Array]'
