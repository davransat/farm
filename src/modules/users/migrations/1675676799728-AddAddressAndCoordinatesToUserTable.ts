import { MigrationInterface, QueryRunner } from "typeorm";

export class AddAddressAndCoordinatesToUserTable1675676799728 implements MigrationInterface {
    name = 'AddAddressAndCoordinatesToUserTable1675676799728'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "address" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "coordinates" geography(Point,4326)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "coordinates"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "address"`);
    }

}
