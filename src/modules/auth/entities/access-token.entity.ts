import { User } from "modules/users/entities/user.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'

@Entity()
export class AccessToken {
  @PrimaryGeneratedColumn("uuid")
  public readonly id: string;

  @Column()
  public token: string;

  @Column()
  public expiresAt: Date;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'userId' })
  public user: User;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}
