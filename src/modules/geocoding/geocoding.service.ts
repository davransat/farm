import { BadRequestError } from '../../errors/errors'
import { Coordinates } from '../../types/coordinates.type'
import { CacheService } from '../cache/cache.service'
import { DistanceMatrixClient } from './clients/distance-matrix/distance-matrix.client'

export class GeocodingService {
  private readonly distanceMatrixClient: DistanceMatrixClient;
  private readonly primitiveCache: CacheService;

  constructor() {
    this.distanceMatrixClient = new DistanceMatrixClient()
    this.primitiveCache = new CacheService()
  }

  async getCoordinatesByAddress(address: string): Promise<Coordinates> {
    const coordinates = await this.distanceMatrixClient.getCoordinates(address)
    if (!coordinates) {
      throw new BadRequestError('Failed to get coordinates')
    }
    return coordinates
  }

  async getTravelDistance(pointA: Coordinates, pointB: Coordinates): Promise<number> {
    const pointsKey = [...pointA, ...pointB].join(',')
    const fromCache = await this.primitiveCache.get(pointsKey)
    if (fromCache) {
      return +fromCache
    }

    const distance = await this.distanceMatrixClient.getTravelDistance(pointA, pointB)

    if (!distance) {
      throw new BadRequestError('Failed to get travel distance')
    }

    await this.primitiveCache.set(pointsKey, `${distance}`)

    return distance
  }
}
