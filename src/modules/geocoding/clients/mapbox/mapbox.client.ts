import mbxGeocoding, { GeocodeService } from '@mapbox/mapbox-sdk/services/geocoding'
import config from '../../../../config/config'
import { BadRequestError } from '../../../../errors/errors'

export class MapboxClient {
  private readonly mbxGeocoding: GeocodeService

  constructor() {
    this.mbxGeocoding = mbxGeocoding({ accessToken: config.MAPBOX_API_KEY });
  }

  public async getGeocodingByAddress (address: string): Promise<number[]> {
    try {
      const { body } = await this.mbxGeocoding.forwardGeocode({ query: address, limit: 1 }).send()
      const [feature] = body.features
      return feature.geometry.coordinates
    }
    catch (e) {
      throw new BadRequestError()
    }
  }
}
