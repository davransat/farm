import fetch from 'node-fetch'
import config from '../../../../config/config'
import { Coordinates } from '../../../../types/coordinates.type'

export class DistanceMatrixClient {

  async getTravelDistance(pointA: Coordinates, pointB: Coordinates): Promise<number> {
    const url = new URL(config.DISTANCE_MATRIX_API_URL + '/distancematrix/json')
    url.searchParams.set('origins', pointA.join(','))
    url.searchParams.set('destinations', pointB.join(','))
    url.searchParams.set('key', config.DISTANCE_MATRIX_API_KEY)
    const result = await fetch(url.href)
    const json = await result.json()
    return json.rows[0]?.elements[0]?.distance.value
  }

  async getCoordinates(address: string): Promise<Coordinates | null> {
    const url = new URL(config.DISTANCE_MATRIX_API_URL + '/geocode/json')
    url.searchParams.set('address', address)
    url.searchParams.set('key', config.DISTANCE_MATRIX_API_KEY)
    const result = await fetch(url.href)
    const json = await result.json()

    if (!json.result[0]) {
      return null
    }

    return [json.result[0].geometry.location.lat, json.result[0].geometry.location.lng]
  }
}
