import { Repository } from 'typeorm'
import dataSource from '../../orm/orm.config'
import { Cache } from './entities/cache.entity'

export class CacheService {
  private readonly cacheRepository: Repository<Cache>;

  constructor() {
    this.cacheRepository = dataSource.getRepository(Cache);
  }

  async has(key: string): Promise<boolean> {
    const count = await this.cacheRepository.countBy({ key })
    return count > 0
  }

  async get(key: string): Promise<string | null> {
    const cacheItem = await this.cacheRepository.findOneBy({ key })
    return cacheItem?.value ?? null
  }

  async set(key: string, value: string): Promise<void> {
    const isExist = await this.has(key)

    if (isExist) {
      await this.cacheRepository.createQueryBuilder()
        .update(Cache)
        .set({ value })
        .where({ key })
        .execute()
    }

    await this.cacheRepository.createQueryBuilder()
      .insert()
      .into(Cache)
      .values({ key, value })
      .execute()
  }
}
