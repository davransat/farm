import { MigrationInterface, QueryRunner } from "typeorm";

export class AddCacheTable1675742084551 implements MigrationInterface {
    name = 'AddCacheTable1675742084551'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "cache" ("key" character varying NOT NULL, "value" character varying, CONSTRAINT "PK_56570efc222b6e6be947abfc801" PRIMARY KEY ("key"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "cache"`);
    }

}
