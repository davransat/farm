import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity()
export class Cache {
  @PrimaryColumn({ unique: true })
  key: string

  @Column({ nullable: true })
  value: string
}
