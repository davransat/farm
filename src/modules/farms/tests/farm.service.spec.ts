import { disconnectAndClearDatabase } from '../../../helpers/db.util'
import ds from '../../../orm/orm.config'
import { GeocodingService } from '../../geocoding/geocoding.service'
import { CreateUserDto } from '../../users/dto/create-user.dto'
import { UsersService } from '../../users/users.service'
import { CreateFarmDto } from '../dto/create-farm.dto'
import { Farm } from '../entities/farm.entity'
import { FarmsService } from '../farms.service'

jest.mock('../../geocoding/geocoding.service', () => {
  return {
    GeocodingService: jest.fn().mockImplementation(() => {
      return {
        getCoordinatesByAddress: () => ([1, 1]),
        getTravelDistance: () => 1
      }
    })
  }
});

describe('FarmsService', () => {
  const mockedGeocodingService = jest.mocked(GeocodingService, {shallow: true})
  let farmsService: FarmsService
  let usersService: UsersService;

  beforeAll(() => {
    mockedGeocodingService.mockClear()
  })

  beforeEach(async () => {
    await ds.initialize();
    usersService = new UsersService();
    farmsService = new FarmsService();
  });

  afterEach(async () => {
    await disconnectAndClearDatabase(ds);
  });

  describe('create farm', () => {
    const createUserDto: CreateUserDto = { email: "user@test.com", password: "password" };
    const createUser = async (userDto: CreateUserDto) => usersService.createUser(userDto);

    it('should create farm', async () => {
      const {id: ownerId} = await createUser(createUserDto)
      const createFarmDto: CreateFarmDto = { name: 'farmName', address: 'Address', size: 1, yield: 1 }
      const farm = await farmsService.createFarm(ownerId, createFarmDto)
      expect(farm).toBeInstanceOf(Farm)
    })
  })

  describe('Delete farm', () => {
    const createUserDto: CreateUserDto = { email: "user@test.com", password: "password" };
    const anotherUserDto: CreateUserDto = { email: "user2@test.com", password: "password" };
    const createUser = async (userDto: CreateUserDto) => usersService.createUser(userDto);

    const createFarmDto: CreateFarmDto = { name: 'farmName', address: 'Address', size: 1, yield: 1 }
    const createFarm = async (ownerId: string, farm: CreateFarmDto) => farmsService.createFarm(ownerId, farm)

    it('should delete farm', async () => {
      const {id: ownerId} = await createUser(createUserDto)
      const { id: farmId } = await createFarm(ownerId, createFarmDto)

      await expect(farmsService.deleteOne(ownerId, farmId)).resolves.not.toThrow()
      expect(await farmsService.findOneById(farmId)).toBeNull()
    })

    it('should throw an error if now an owner tries to delete', async () => {
      const {id: ownerId} = await createUser(createUserDto)
      const {id: anotherUserId} = await createUser(anotherUserDto)
      const { id: farmId } = await createFarm(ownerId, createFarmDto)

      await expect(farmsService.deleteOne(anotherUserId, farmId)).rejects.toThrow()
      expect(await farmsService.findOneById(farmId)).toBeInstanceOf(Farm)
    })
  })

  describe('getAll', () => {
    const createUserDto: CreateUserDto = { email: "user@test.com", password: "password" };
    const anotherUserDto: CreateUserDto = { email: "user2@test.com", password: "password" };
    const createUser = async (userDto: CreateUserDto) => usersService.createUser(userDto);

    const createFarm1Dto: CreateFarmDto = { name: 'CfarmName', address: 'Address', size: 1, yield: 1 }
    const createFarm2Dto: CreateFarmDto = { name: 'AfarmName', address: 'Address', size: 1, yield: 5 }
    const createFarm3Dto: CreateFarmDto = { name: 'BfarmName', address: 'Address', size: 1, yield: 10 }

    const createFarm = async (ownerId: string, farm: CreateFarmDto) => farmsService.createFarm(ownerId, farm)

    it('should get all farms', async () => {
      const {id: ownerId} = await createUser(createUserDto)
      const {id: anotherUserId} = await createUser(anotherUserDto)
      await createFarm(ownerId, createFarm1Dto)
      await createFarm(anotherUserId, createFarm2Dto)
      await createFarm(anotherUserId, createFarm3Dto)

      const allFarms = await farmsService.getAllWithDrivingDistance([1, 1])
      expect(allFarms.length).toBe(3)
    })

    it('should get farms with outliers', async () => {
      const {id: ownerId} = await createUser(createUserDto)
      const {id: anotherUserId} = await createUser(anotherUserDto)
      await createFarm(ownerId, createFarm1Dto)
      const {id: farmId2} = await createFarm(anotherUserId, createFarm2Dto)
      await createFarm(anotherUserId, createFarm3Dto)

      const allFarms = await farmsService.getAllWithDrivingDistance([1, 1], {outliers: true})
      expect(allFarms.length).toBe(2)
      expect(allFarms.some(({id}) => id === farmId2)).toBeFalsy()
    })

    it('should get all farms ordered by name a-z', async () => {
      const {id: ownerId} = await createUser(createUserDto)
      const {id: anotherUserId} = await createUser(anotherUserDto)
      const {id: farmId1} = await createFarm(ownerId, createFarm1Dto)
      const {id: farmId2} = await createFarm(anotherUserId, createFarm2Dto)
      const {id: farmId3} = await createFarm(anotherUserId, createFarm3Dto)

      const allFarms = await farmsService.getAllWithDrivingDistance([1, 1], {name: true})
      expect(allFarms.length).toBe(3)
      expect(allFarms[0].id).toBe(farmId2)
      expect(allFarms[1].id).toBe(farmId3)
      expect(allFarms[2].id).toBe(farmId1)
    })

    it('should get all farms ordered by creation date', async () => {
      const {id: ownerId} = await createUser(createUserDto)
      const {id: anotherUserId} = await createUser(anotherUserDto)
      const {id: farmId1} = await createFarm(ownerId, createFarm1Dto)
      const {id: farmId2} = await createFarm(anotherUserId, createFarm2Dto)
      const {id: farmId3} = await createFarm(anotherUserId, createFarm3Dto)

      const allFarms = await farmsService.getAllWithDrivingDistance([1, 1], {date: true})
      expect(allFarms.length).toBe(3)
      expect(allFarms[0].id).toBe(farmId3)
      expect(allFarms[1].id).toBe(farmId2)
      expect(allFarms[2].id).toBe(farmId1)
    })
  })
})
