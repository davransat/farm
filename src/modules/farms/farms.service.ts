import { Point } from 'geojson'
import { Repository } from "typeorm";
import dataSource from "orm/orm.config";
import { BadRequestError } from '../../errors/errors'
import { isNumber } from '../../helpers/check-types/check-types.util'
import { Coordinates } from '../../types/coordinates.type'
import { GeocodingService } from '../geocoding/geocoding.service'
import { CreateFarmDto } from './dto/create-farm.dto'
import { GetAllFarmsRequestQueryDto } from './dto/get-all-farms-request-query.dto'
import { GetFarmDto } from './dto/get-farm.dto'
import { Farm } from './entities/farm.entity'

export class FarmsService {
  private readonly farmRepository: Repository<Farm>;
  private readonly geocodingService: GeocodingService;

  constructor() {
    this.farmRepository = dataSource.getRepository(Farm);
    this.geocodingService = new GeocodingService()
  }

  public async createFarm(ownerId: string, data: CreateFarmDto): Promise<Farm> {
    const coordinates = await this.geocodingService.getCoordinatesByAddress(data.address)
    if (!coordinates) {
      throw new BadRequestError(`Failed to get coordinates of address: ${data.address}`)
    }

    const pointObject: Point = {
      type: "Point",
      coordinates: coordinates,
    }

    const newFarm = this.farmRepository.create({
      ...data,
      ownerId,
      coordinates: pointObject
    });

    return this.farmRepository.save(newFarm);
  }

  public async deleteOne(ownerId: string, farmId: string): Promise<void> {
    const deleteResult = await this.farmRepository.createQueryBuilder('farm')
      .delete()
      .from(Farm)
      .where('farm.id = :farmId', { farmId })
      .andWhere('farm."ownerId" = :ownerId', { ownerId })
      .execute()

    if (!isNumber(deleteResult.affected) || deleteResult.affected < 1) {
      throw new BadRequestError(`Failed to delete the farm with id: ${farmId}`)
    }
  }

  public async getAllWithDrivingDistance(userCoordinates: Coordinates, filterAndSort: GetAllFarmsRequestQueryDto = {}): Promise<GetFarmDto[]> {
    const farms = await this.getAll(filterAndSort)

    const withDistance = await Promise.all(farms.map(async farm => {
      const drivingDistance = await this.geocodingService.getTravelDistance(userCoordinates, farm.coordinates.coordinates as Coordinates)

      return {
        id: farm.id,
        name: farm.name,
        address: farm.address,
        owner: farm.owner.email,
        size: farm.size,
        yield: farm.yield,
        drivingDistance
      }
    }))

    if(filterAndSort.drivingDistance) {
      return withDistance.sort((a, b) => a.drivingDistance - b.drivingDistance)
    }

    return withDistance
  }

  findOneById(id: string): Promise<Farm | null> {
    return this.farmRepository.findOneBy({ id })
  }

  findAll(): Promise<Farm[]> {
    return this.farmRepository.createQueryBuilder('farm')
      .leftJoinAndSelect('farm.owner', 'owner')
      .getMany()
  }

  private async getAll (filterAndSort: GetAllFarmsRequestQueryDto = {}): Promise<Farm[]> {
    const qb = this.farmRepository.createQueryBuilder('farm')
      .leftJoin('farm.owner', 'owner')
      .addSelect('owner.email')


    if(filterAndSort.outliers) {
      const { averageYield } = await this.farmRepository.createQueryBuilder('farm')
        .select('AVG(farm.yield)', 'averageYield')
        .getRawOne()

      qb.where((`(farm.yield < (:averageYield - (:averageYield * 0.3))) OR (farm.yield > (:averageYield + (:averageYield * 0.3)))`), {
        averageYield,
      })
    }

    if(filterAndSort.name) {
      qb.orderBy('farm.name', 'ASC')
    }

    if(filterAndSort.date) {
      qb.orderBy('farm.createdAt', 'DESC')
    }


    return qb.getMany()
  }

}
