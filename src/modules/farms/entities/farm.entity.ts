import { Point } from 'geojson'
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { User } from '../../users/entities/user.entity'

@Entity()
export class Farm {
  @PrimaryGeneratedColumn("uuid")
  public readonly id: string;

  @Column()
  public name: string;

  @Column()
  public address: string;

  @Column({
    type: 'geography',
    spatialFeatureType: 'Point',
    srid: 4326,
  })
  public coordinates: Point;

  @Column({ type: 'real' })
  public size: number;

  @Column({ type: 'real' })
  public yield: number;

  @ManyToOne(() => User, user => user.farms)
  @JoinColumn({ name: 'ownerId' })
  public owner: User

  @Column({ select: false })
  public ownerId: string

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}
