import { Transform } from 'class-transformer'
import { Point } from 'geojson'

export class FarmDto {
  constructor(partial?: Partial<FarmDto>) {
    Object.assign(this, partial);
  }

  public id: string;
  public name: string;
  public address: string;
  public coordinates: Point;
  public size: number;
  public yield: number;
  public owner: string

  @Transform(({ value }) => (value as Date).toISOString())
  public createdAt: Date;

  @Transform(({ value }) => (value as Date).toISOString())
  public updatedAt: Date;

  public static createFromEntity(farm: FarmDto | null): FarmDto | null {
    if (!farm) {
      return null;
    }

    return new FarmDto({ ...farm });
  }
}
