import { Transform } from 'class-transformer'
import { IsBoolean, IsOptional } from 'class-validator'
import { stringToBoolean } from '../../../helpers/string-to-boolean/string-to-boolean.util'

export class GetAllFarmsRequestQueryDto {
  @IsBoolean()
  @IsOptional()
  @Transform(({value}) => stringToBoolean(value))
  name?: boolean

  @IsBoolean()
  @IsOptional()
  @Transform(({value}) => stringToBoolean(value))
  date?: boolean

  @IsBoolean()
  @IsOptional()
  @Transform(({value}) => stringToBoolean(value))
  drivingDistance?: boolean

  @IsBoolean()
  @IsOptional()
  @Transform(({value}) => stringToBoolean(value))
  outliers?: boolean
}
