export class GetFarmDto {
  id: string
  name: string
  address: string
  owner: string
  size: number
  yield: number
  drivingDistance: number
}
