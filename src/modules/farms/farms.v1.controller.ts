import { plainToInstance } from 'class-transformer'
import { validateOrReject } from 'class-validator'
import { NextFunction, Response } from "express";
import { AuthorizedRequest } from '../../types/authorized-request.type'
import { Coordinates } from '../../types/coordinates.type'
import { CreateFarmDto } from './dto/create-farm.dto'
import { FarmDto } from './dto/farm.dto'
import { GetAllFarmsRequestQueryDto } from './dto/get-all-farms-request-query.dto'
import { FarmsService } from "./farms.service";

export class FarmsV1Controller {
  private readonly farmsService: FarmsService;


  constructor() {
    this.farmsService = new FarmsService();
  }

  public async createOne(req: AuthorizedRequest, res: Response, next: NextFunction) {
    try {
      const farm = await this.farmsService.createFarm(req.user.id, req.body as CreateFarmDto);
      res.status(201).send(FarmDto.createFromEntity({ ...farm, owner: farm.owner?.email }));
    } catch (error) {
      next(error);
    }
  }

  public async deleteOne(req: AuthorizedRequest, res: Response, next: NextFunction) {
    try {
      await this.farmsService.deleteOne(req.user.id, req.params['id']);
      res.status(200).send;
    } catch (error) {
      next(error);
    }
  }

  public async getAll (request: AuthorizedRequest, res: Response, next: NextFunction): Promise<void> {
    try {
      const queryParams = plainToInstance(GetAllFarmsRequestQueryDto, request.query)
      await validateOrReject(queryParams)

      const allFarms = await this.farmsService.getAllWithDrivingDistance(
        request.user.coordinates?.coordinates! as Coordinates,
        queryParams
      );

      res.status(201).send(allFarms);
    } catch (error) {
      next(error);
    }
  }
}
