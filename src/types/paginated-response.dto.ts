export type PaginatedResponseDto<T> = {
  items: T[]
  count: number
}
