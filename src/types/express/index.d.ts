import { User } from '../../modules/users/entities/user.entity'

export {}

declare global {
  namespace Express {
    export interface Request {
      user?: User;
    }
  }
}
