import { NextFunction, Request, Response } from 'express'
import { verify } from "jsonwebtoken";
import config from "config/config";
import { UnauthorizedError } from '../errors/errors'
import { AuthService } from '../modules/auth/auth.service'
import { User } from '../modules/users/entities/user.entity'

export type JwtPayload = {
  id: string
  email: string
  iat: number
  exp: number
}

const authService = new AuthService()

export const authMiddleware = async (request: Request, _: Response, next: NextFunction): Promise<void> => {
  const [, accessToken] = request.headers['authorization']?.split(' ') ?? []
  try {
    const jwtPayload = getJWTPayload(accessToken, config.JWT_SECRET)
    request.user = await getUserByValidToken(jwtPayload, accessToken)
    next()
  } catch (e) {
    next(e)
  }
}

const getJWTPayload = (accessToken: string | null | undefined, jwtSecret: string): JwtPayload => {
  if (!accessToken) {
    throw new UnauthorizedError()
  }
  try {
    return verify(accessToken, jwtSecret) as JwtPayload
  } catch (e) {
    throw new UnauthorizedError()
  }
}

const getUserByValidToken = async (jwtPayload: JwtPayload, accessToken: string): Promise<User> => {
  return authService.getUserByValidToken(jwtPayload, accessToken)
}
