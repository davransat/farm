import { ValidationError } from 'class-validator'
import { UnauthorizedError, UnprocessableEntityError } from 'errors/errors'
import { NextFunction, Request, Response } from "express";
import { isArray } from '../helpers/check-types/check-types.util'

export function handleErrorMiddleware(error: Error | ValidationError[], _: Request, res: Response, next: NextFunction): void {
  if (error instanceof UnprocessableEntityError) {
    const { message } = error;
    res.status(422).send({ name: "UnprocessableEntityError", message });
  } else if (isArray(error) &&  error[0] instanceof ValidationError) {
    const errorMessages: string[] = []
    error.forEach(error => Object.values(error.constraints ?? {}).forEach(errorMessage => errorMessages.push(`${errorMessage}`)));
    res.status(400).send({ name: "BadRequestError", message: errorMessages });
  } else if (error instanceof UnauthorizedError) {
    res.status(401).send('Unauthorized')
  } else {
    res.status(500).send({ message: "Internal Server Error" });
  }

  next();
}
