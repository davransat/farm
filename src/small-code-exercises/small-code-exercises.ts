import * as fs from 'fs'


export const isString = (arg: any): arg is number => Object.prototype.toString.call(arg) === '[object String]'
export const isNumber = (arg: any): arg is number => Object.prototype.toString.call(arg) === '[object Number]'

export const transformArray = (arr: any[]): (string | number)[] => {
  return arr.reduce<(string | number)[]>((acc, cur) => {
    if (!(isString(cur) || isNumber(cur))) {
      return acc
    }
    const value = Number.isNaN(+cur) ? cur : +cur
    return [...acc, value]
  }, [])
}

// const directoryPath = path.join(__dirname, '../', 'files');

export const getFilesByExtension = async (directoryPath: string, extension: string): Promise<string[]> => {
  return new Promise((resolve, reject) => {
    fs.readdir(directoryPath, function (err: NodeJS.ErrnoException, files: string[] ) {
      if (err || files === undefined) {
        reject('Unable to read directory: ' + err)
      }
      const csvFiles = files.filter(file => file.substring(file.lastIndexOf('.') + 1, file.length) === extension)
      resolve(csvFiles)
    });
  })

}

export const isStringContainsDigits = (str: string): boolean => {
  return /\d/.test(str);
}
