import * as path from 'path'
import { getFilesByExtension, isStringContainsDigits, transformArray } from './small-code-exercises'

describe.only('Test small-code-exercises', () => {
  describe('transformArray', () => {
    it('should return array of string and numbers', () => {
      const mixedArray = [true, 1, 'one', '1', '1aw', '20.5', 20.5]
      const expectedResult = [1, 'one', 1, '1aw', 20.5, 20.5]
      expect(transformArray(mixedArray)).toStrictEqual(expectedResult)
    })
  })

  describe('getFilesByExtension', () => {
    const directoryPath = path.join(__dirname, '../../', 'files');
    it('should return a list of filenames with csv extension', async () => {
      const expectedResult = ['export.csv', 'import.csv']

      const response = await getFilesByExtension(directoryPath, 'csv')
      expect(response.length).toBe(expectedResult.length)
      for (const filename of response) {
        expect(expectedResult.indexOf(filename) > -1).toBeTruthy()
      }
    })
  })

  describe('isStringContainsDigits', () => {
    it('should return falsy response', () => {
      const str = 'test-string'
      expect(isStringContainsDigits(str)).toBeFalsy()
    })

    it('should return truthy response', () => {
      const str = 'test-string23'
      expect(isStringContainsDigits(str)).toBeTruthy()
    })
  })
})
