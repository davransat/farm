import { InsertResult } from 'typeorm'
import { Farm } from '../src/modules/farms/entities/farm.entity'
import { User } from '../src/modules/users/entities/user.entity'
import dataSource from '../src/orm/orm.config'
import ds from '../src/orm/orm.config'
import { faker } from '@faker-js/faker';

export const seeds = async () =>{
  await ds.initialize();
  const users = await seedUsers(4)
  await Promise.all(users.map(user => seedFarms(user.identifiers[0].id, 30)))
}

const seedUsers = async (count: number): Promise<InsertResult[]> => {
  const usersRepository = dataSource.getRepository(User);
  const createUser = (dto: Pick<User, 'email' | 'hashedPassword' | 'coordinates'>) => {
    return usersRepository.createQueryBuilder()
      .insert()
      .into(User)
      .values(dto)
      .execute()
  }
  const userCreateDtos: Pick<User, 'email' | 'hashedPassword' | 'coordinates'>[] = []
  for (let i = 0; i < count; i++) {
    userCreateDtos.push({
      email: faker.internet.email(),
      hashedPassword: faker.internet.password(64),
      coordinates: {
        type: 'Point',
        coordinates: faker.address.nearbyGPSCoordinate([51.499381549999995, -0.1275749]).map(it => +it)
      }
    })
  }
  return Promise.all(userCreateDtos.map(createUser))
}
const seedFarms = async (ownerId: string, count: number): Promise<InsertResult[]> => {
  const usersRepository = dataSource.getRepository(Farm);
  const createFarm = (dto: Pick<Farm, 'name' | 'address' | 'coordinates' | 'ownerId' | 'size' | 'yield'>) => {
    return usersRepository.createQueryBuilder()
      .insert()
      .into(Farm)
      .values(dto)
      .execute()
  }
  const farmCreateDtos: Pick<Farm, 'name' | 'address' | 'coordinates' | 'ownerId' | 'size' | 'yield'>[] = []
  for (let i = 0; i < count; i++) {
    farmCreateDtos.push({
      name: faker.random.word(),
      address: faker.address.streetAddress(),
      coordinates: {
        type: 'Point',
          coordinates: faker.address.nearbyGPSCoordinate([51.499381549999995, -0.1275749]).map(it => +it)
      },
      ownerId,
      size: faker.datatype.float(),
      yield: faker.datatype.float()
    })
  }
  return Promise.all(farmCreateDtos.map(createFarm))
}



seeds()
